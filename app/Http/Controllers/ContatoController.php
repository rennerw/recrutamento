<?php

namespace App\Http\Controllers;

use App\Contato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use DB;
use Illuminate\Support\Facades\Log;
Use Alert;
class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = Contato::paginate(10);

        return view('contato.index')->with("contatos", $contatos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contato.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required|string|min:6|max:255',
            'contato' => 'required|string|max:9|unique:contatos',
            'email' => 'nullable|email|max:255|unique:contatos'
        ]);

        DB::beginTransaction();
        try{
           
            $contato = Contato::create($request->all());
            DB::commit();
            Alert::success("Contato criada com sucesso");
            return redirect()->route('contato.index');
        }
        catch(\Exception $e){            
            DB::rollback();
            Log::error('Falha ao criar contato '.$e->getCode()." Msg: ".$e->getMessage());
            return abort('500');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contato  $contato
     * @return \Illuminate\Http\Response
     */
    public function show(Contato $contato)
    {
        return view('contato.show', compact('contato'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contato  $contato
     * @return \Illuminate\Http\Response
     */
    public function edit(Contato $contato)
    {
        return view('contato.edit', compact('contato'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contato  $contato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contato $contato)
    {
        $request->validate([
            'nome' => 'required|string|min:6|max:255',
            'contato' => [
                'required',
                'string',
                'max:9',
                \Illuminate\Validation\Rule::unique('contatos')->ignore($contato->id)
            ],
            'email' => [
                'email',
                'max:255',
                \Illuminate\Validation\Rule::unique('contatos')->ignore($contato->id)
            ]
        ]);

        DB::beginTransaction();
        try{
           
            $contato->update($request->all());

            DB::commit();
            Alert::success("Contato editado com sucesso");
            return redirect()->route('contato.index');
        }
        catch(\Exception $e){            
            DB::rollback();
            Log::error('Falha ao criar Contato '.$e->getCode()." Msg: ".$e->getMessage());
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contato  $contato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contato $contato)
    {
        DB::beginTransaction();
        try{
            
            $contato->delete();
            DB::commit();

            Alert::success("Contato removido com sucesso");
            return redirect()->route('contato.index');
        }
        catch(\Exception $e){            
            DB::rollback();
            Log::error('Falha ao apagaar contato '.$e->getCode()." Msg: ".$e->getMessage());
            return abort('500');
        }
    }

    public function showContactsToGuest(Request $request){
        $contatos = Contato::paginate(10);

        return view('contato.convidados')->with("contatos", $contatos);
    }
}
