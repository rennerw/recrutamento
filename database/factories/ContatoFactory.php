<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contato;
use Faker\Generator as Faker;

use Faker\Provider\pt_BR\Person;
use Faker\Provider\pt_BR\PhoneNumber;
use Faker\Provider\Internet;
$factory->define(Contato::class, function (Faker $faker) {
    $faker->addProvider(new Person($faker));
    $faker->addProvider(new Internet($faker));
    $faker->addProvider(new PhoneNumber($faker));

    $nome = "{$faker->firstName} {$faker->lastName}";
    //* Remove possíveis acentos dos nomes em portugues */
    $str = mb_strtolower(explode(' ', $nome)[0]);
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    $str = preg_replace('/[\.]/ui', '', $str);

    $email = $str.Str::random(2)."@teste.com";
    return [
        'nome' => $nome,
        'contato' => Str::random(9),
        'email' => $email
    ];
});
