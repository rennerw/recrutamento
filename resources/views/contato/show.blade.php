@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Ver Contato</div>
    <div class="card-body">
    <a class="" href="{{route('contato.index')}}">
        Voltar para lista
    </a>
    <div class="h-100"></div>
    <a class="btn btn-success" href="{{route('contato.edit', $contato->id)}}">
        Editar
    </a>
    <form method="POST" class="d-inline" action="{{route('contato.destroy',$contato->id)}}">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger">Excluir</button>
    </form>
    <div class="row mt-4">
        <div class="col-6">
            <h2><b>Criado em</b></h2>
            <h3>{{$contato->created_at}}</h3>
        </div>
        <div class="col-6">
            <h2><b>Atualizado em</b></h2>
            <h3>{{$contato->updated_at}}</h3>
        </div>
        <div class="col-12">
            <h2><b>Nome do Contato</b></h2>
            <h3>{{$contato->nome}}</h3>
        </div>
        <div class="col-12">
            <h2><b>Contato</b></h2>
            <h3>{{$contato->contato}}</h3>
        </div>
        <div class="col-12">
            <h2><b>Email</b></h2>
            <h3>{{$contato->email}}</h3>
        </div>
    </div>
    </div>
</div>
@endsection