@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Contatos</div>
    <div class="card-body">
    <h2>Veja os contatos</h2>
        <table class="table">
            <thead class="">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nome</th>
                <th scope="col">Contato</th>
                <th scope="col">Email</th>
              </tr>
            </thead>
            <tbody>
                @foreach($contatos as $contato) 
                <tr>
                    <td>{{$contato->id}}</td>
                    <td>{{$contato->nome}}</td>
                    <td>{{$contato->contato}}</td>
                    <td>{{$contato->email}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>{{ $contatos->links() }}</tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection