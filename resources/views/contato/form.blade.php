<div class="row">
    @csrf
    <div class="col-md-12">
        <div class="form-group">
            <label for="nome" class="control-label">Nome</label>
            <input id="nome" type="text" class="form-control @error('nome')is-invalid @enderror" name="nome" value="{{ old('nome') ?? $contato->nome ?? '' }}" required autofocus>
            @error('nome')
                <div style="color: red;" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="contato" class="control-label">Contato</label>
            <input id="contato" type="text" class="form-control  @error('contato')is-invalid @enderror" name="contato" value="{{ old('contato') ?? $contato->contato ?? '' }}" required maxlength="9">
            @error('contato')
                <div style="color: red;" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="email" class="control-label">Email</label>
            <input id="email" type="email" class="form-control @error('email')is-invalid @enderror" name="email" value="{{ old('email') ?? $contato->email ?? '' }}">
            @error('email')
                <div style="color: red;" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
            @enderror
        </div>
    </div>
</div>