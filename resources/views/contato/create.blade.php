@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Criar Contato</div>
    <div class="card-body">
    <a class="" href="{{route('contato.index')}}">
        Voltar para lista
    </a>
    
    <form method="POST" action="{{ route('contato.store') }}" accept-charset="UTF-8" enctype="multipart/form-data" class="mt-3">
        @csrf

        @include("contato.form")

        <div class="text-center">
            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Salvar</button>
            <a href="{{ route('contato.index') }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar</a>
        </div>
    </form>
    </div>
</div>
@endsection