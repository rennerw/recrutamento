@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Contatos</div>
    <div class="card-body">
    <a class="btn btn-primary mb-4" href="{{route('contato.create')}}">
        <i class="fa fa-plus"></i>
        Adicionar
    </a>
    <h2>Veja, edite e remova os contatos</h2>
        <table class="table">
            <thead class="">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Nome</th>
                <th scope="col">Contato</th>
                <th scope="col">Email</th>
                <th scope="col">Ações</th>
              </tr>
            </thead>
            <tbody>
                @foreach($contatos as $contato) 
                <tr>
                    <td>{{$contato->id}}</td>
                    <td>{{$contato->nome}}</td>
                    <td>{{$contato->contato}}</td>
                    <td>{{$contato->email}}</td>
                    <td>
                        <a href="{{route('contato.show', $contato->id)}}" class="btn btn-secondary" title="Editar contato">Ver Detalhes</a>
                        <a href="{{route('contato.edit', $contato->id)}}" class="btn btn-success" title="Editar contato">Editar</a>
                        <form class="d-inline" method="POST" action="{{route('contato.destroy',$contato->id)}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Excluir</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>{{ $contatos->links() }}</tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection