@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Editar Contato</div>
    <div class="card-body">
    <a class="" href="{{route('contato.index')}}">
        Voltar para lista
    </a>
    <div class="h-100"></div>
    <form method="POST" class="d-inline" action="{{route('contato.destroy',$contato->id)}}">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger">Excluir</button>
    </form>
    <form method="POST" action="{{ route('contato.update', $contato->id) }}" accept-charset="UTF-8" enctype="multipart/form-data" class="mt-3">
        @csrf
        @method('PUT')
        @include("contato.form")

        <div class="text-center">
            <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Salvar</button>
            <a href="{{ route('contato.index') }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Voltar</a>
        </div>
    </form>
    </div>
</div>
@endsection